#include <GL/freeglut.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <Windows.h>
#include <mmsystem.h>    
#pragma comment(lib,"winmm.lib") 


#define SOUND_FILE_BGM "resource\\Energy Drink.wav"
#define SOUND_FILE_BGM2 "resource\\GTA5.wav"
#define SOUND_FILE_BGM3 "resource\\ants.wav"

#pragma warning(disable:4996);
#define NUM 1000
#define RANK_LIM 100
#define TEXTURE_NUMBER 20
typedef struct Road {
	int x;
	int z;
}sRoad;

GLubyte * TexBits;
BITMAPINFO *info;
GLubyte *pBytes;
GLuint textures[TEXTURE_NUMBER];

int road[NUM][5];//0 =  구덩이,1 = 길,2 = 출튀(점프) ,3 = 숙제(빨강) ,4 = 휴강(파랑)  ,5 = ,6 = 축제 타일(보라) ,7 = 술(황토)
bool road_on[NUM][5]; // 이미 충돌체크한 애들은 충돌체크 끄는 배열( 점수때 씀)
GLvoid drawScene(GLvoid);
GLvoid Reshape(int w, int h);

GLvoid menuScreen(GLvoid);

sRoad drawRoad[NUM][5];
////////////////////////////////////////////////////////////////////변수
bool ForTest = false;

int TileInterval = 42;
int set = 0;   // 도로입력 확인용

int menu = 0;   // 메뉴 ( 0 = 초기화면 / 1 = 게임화면 )
float level = 1;   // 레벨 ( 1 = 18학점 / 2 = 21학점 / 3 = 24학점) Easy,Normal,Hard
float SpeedTemp = 0; // 도로 속도조절변수
int Life = 0;   // 목숨
float Distance = 0; // 교수님과 학생사이 거리

int moveX;   // 캐릭터 x값 확인 변수
float charX = 0;// 캐릭터 x 위치
float charZ = 0;

float MoveRoad = 0;   // 시간 증가
float MoveWire = 0;
float StudentAngle = 0;   //팔,다리,머리흔드는 각도
int StudentTemp = 0;   //학생 각도 확인 변수
float Fest_Student_Angle = 0;   // 축제시 날라가는 학생 각도
bool Jump_on = false, Jump_bool = false;   //캐릭터 점프 변수
float Jump = 0;   // 점프 위치
bool survive;   // 캐릭터 생존여부
float Count = 0;   // 밟은 발판 개수
bool Clear = false;
float drink = 0; //마신 술 개수

bool Item_Homework = false;   // 과제아이템 획득여부.
bool Item_Run = false;      // 출튀아이템 획득여부.
bool Item_Class_Cancel = false; // 휴강아이템 획득여부.
bool Item_Fest = false;      // 축제아이템 획득여부.
bool Fest_bool = false;      // 축제기간.

bool RideWire = false;      // 줄타기구간 입장여부
bool RideWire_on = false;   // 줄 획득여부
bool MoveWire_bool = false;

int Hour;//시
int Min;//분
int Sec;//초

bool View = false;
sRoad Point;

int rank_num = 0;//랭킹에 등록된 사람수
int Rank_Score[RANK_LIM];
int Rank_grade[RANK_LIM];
bool Rank_bool = true;  //끝나고 참일때만 점수 계산
bool Rank_Rect = false; //참일때만 학점 표시
int RANK = 0; //0 =f 1= d0 2=d+ 3=c0 4=c+ 5=b0 6=b+ 7=a0 8=a+ 

FILE *rfp, *wfp, *rfp2, *wfp2; //파일 입출력 함수

int Start_count = 0;
bool Start_on = false;
bool First_menu = true;
float Text_score_S[6];//5=십만 4=만 3=천 2=백 1=십 0=일
int endSpin_y = 0, endTrans_x = 0, endTrans_y = 0, endTrans_z = 0; //끝날때 애니메이션
int SCORE = 0;   //점수 글 출력으로 오른쪽 상단에 띄울예정  //0.1초에 12점 ,  파랑색 +5000, 빨간색 -2000, 노랑 +3000, 한칸 지날때마다 +100,축제 타일 밟으면 +1000 ,축제 기간에는 타일 점수 2배, 술먹으면 1000점
				 ///////////////////////////////////////////////////////////////함수
void Mouse(int button, int state, int x, int y);
void SpecialKeyboard(int key, int x, int y);
void keyboard(unsigned char key, int x, int y);//키보드
void Make_menu(); // 메뉴판
void Roadsetting(); // 길 배열정보 입력
void RandomRoad(); // 장애물 랜덤설정
void Student();      // 학생
void Fest_Student();   // 학생 축제모션
void Wire_Student();      // 학생 줄타기모션
void Teacher();      // 선생님
void Texture_make();
GLubyte * LoadDIBitmap(const char *filename, BITMAPINFO **info);

void RoadTimer(int value);  // 도로속도조절
void CharTimer(int value);  // 캐릭터속도조절
void MenuTimer(int value);  // 메뉴 선택확인용
void JumpTimer(int value);   // 점프속도 조절용

void Road();      // 길
void collide();      // 충돌체크
void gameStart();   // 시작시 초기화
void Progress();   // 진행률(상단표시)
void DrunkenBar();   // 숙취게이지
void End();         // 끝
void TimeTimer(int value);
void Viewfunc();
void Drawtext(int x, int y, int z, char string[]);
void FileInput();//파일 불러오기
void FileOutput();//파일 내보내기
void Score(); // 화면에 점수 표시
void Rank_Func(); //점수에 따라 학점부여 함수
void Rank_View(); //화면에 학점 표시
void T_light();
////////////////////////////////////////////////////////////////
int inRect(int x, int y, int rectx1, int recty1, int rectx2, int recty2); // 꼭지점,사각형 충돌체크

int main(int argc, char* argv[])
{

	Roadsetting();
	FileInput();
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH); // 디스플레이 모드 설정 
	glutInitWindowPosition(100, 100); // 윈도우의 위치지정 
	glutInitWindowSize(800, 600); // 윈도우의 크기 지정
	glutCreateWindow("RunGame");// 윈도우 생성 (윈도우 이름)
	glutDisplayFunc(drawScene);// 출력 함수의 지정   
	Texture_make();
	glutTimerFunc(10, RoadTimer, 1);
	glutTimerFunc(5, CharTimer, 1);
	glutTimerFunc(100, MenuTimer, 1);
	glutTimerFunc(10, JumpTimer, 1);
	glutTimerFunc(100, TimeTimer, 2);
	glutMouseFunc(Mouse);
	glutSpecialFunc(SpecialKeyboard);
	glutKeyboardFunc(keyboard);
	glutReshapeFunc(Reshape);
	glutMainLoop();

}// 윈도우 출력 함수
GLvoid drawScene(GLvoid)//그리기
{

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // 바탕색을 'black' 로 지정
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // 설정된 색으로 전체를 칠하기
	glEnable(GL_DEPTH_TEST);
	//PlaySound(NULL, 0, 0);

	if (menu == 0) {

		Make_menu();
		glPushMatrix();
		glTranslatef(-250, -100, 200);
		glScalef(2, 2, 2);
		glRotatef(180, 0, 1, 0);
		Fest_Student();
		glPopMatrix();
		glEnd();
	}
	else if (menu == 1) {
		
		
		Progress();
		glPushMatrix();
		Score();
		if (!RideWire)   glRotatef(3 * moveX, 0, 1, 0); // 학생 위치에 따라 맵 전체회전
		else if (RideWire)   glRotatef(MoveWire / 60, 0, 1, 0);
		glPushMatrix();
		glEnable(GL_TEXTURE_2D);
		glColor3f(0.8, 0.8, 0.8);

		glBindTexture(GL_TEXTURE_2D, textures[3]);
		glTranslatef(0, 0, -5000);
		glBegin(GL_QUADS);//정면
		glTexCoord2f(0, 1);
		glVertex2f(-8000, 4000);
		glTexCoord2f(1, 1);
		glVertex2f(8000, 4000);
		glTexCoord2f(1, 0);
		glVertex2f(8000, -5000);
		glTexCoord2f(0, 0);
		glVertex2f(-8000, -5000);
		glEnd();
		glDisable(GL_TEXTURE_2D);
		glPopMatrix();


		if (survive == false || Clear)
		{
			if (Rank_Rect || Clear)
			{
				Rank_View();
			}

			glRotatef(endSpin_y, 0, 1, 0); //끝날때 회전
			glTranslatef(endTrans_x, endTrans_y, endTrans_z);//끝날때 회전
		}
		Road();      //도로 & 아이템 그리기
		if (!Clear) {
			if (Item_Run)   Fest_Student();   //학생 그리기
			else if (RideWire) Wire_Student();
			else Student();
		}
		else {
			glPushMatrix();
			glTranslatef(0, -80, 100);
			glRotatef(180, 0, 1, 0);
			glTranslatef(0, 80, -100);
			Fest_Student();
			glPopMatrix();
		}
		if (!RideWire) {
			Teacher();
			glPushMatrix();
			glTranslatef(15, 0, 10);
			Teacher();
			glPopMatrix();

			glPushMatrix();
			glTranslatef(-15, 0, 10);
			Teacher();
			glPopMatrix();
		}
		glPopMatrix();

		// 배경
		if (Fest_bool) {
			DrunkenBar();
		}

	}
	else if (menu == 2)
	{
		glPushMatrix();
		int Temp;
		glTranslatef(-280, 150, 0);

		for (int i = 0; i < 18; i++)
		{

			int num = 0;
			for (int j = 0; j < 6; j++)   Text_score_S[j] = 0;
			Temp = Rank_Score[i];
			while (Temp != 0)//자리수 나누기
			{
				Text_score_S[num] = Temp % 10;
				Temp /= 10;
				num += 1;
			}
			if (i == 9)glTranslatef(330, 590, 0);
			glTranslatef(0, -65, 0);
			glPushMatrix();


			glPopMatrix();
			Score();

		}
		glBindTexture(GL_TEXTURE_2D, textures[15]);
		glEnable(GL_TEXTURE_2D);
		glPopMatrix();
		glColor3f(1, 1, 1);
		glBegin(GL_QUADS);
		glTexCoord2f(0, 1);
		glVertex2f(-300, 280);
		glTexCoord2f(1, 1);
		glVertex2f(300, 280);
		glTexCoord2f(1, 0);
		glVertex2f(300, -280);
		glTexCoord2f(0, 0);
		glVertex2f(-300, -280);
		glEnd();
		glDisable(GL_TEXTURE_2D);
	}
	glutSwapBuffers();
}
void SpecialKeyboard(int key, int x, int y)
{
	switch (key)
	{


	default:
		break;
	}
}
void keyboard(unsigned char key, int x, int y)
{
	switch (key) {
	case '+':
		SpeedTemp += 1;
		break;
	case '-':
		if (SpeedTemp > level) {
			SpeedTemp -= 1;
		}
		break;

	case 'w':
		if (survive != false) {
			if (MoveRoad >= 28010 && MoveRoad < 32000)   RideWire_on = true;
			else Jump_on = true;
			MoveWire = 0;
			RideWire = false;
		}
		break;
	case 'a':
		if (survive != false) {
			moveX -= 1;
			if (moveX < -2) moveX += 1;
		}
		break;
	case 'q':
		if (survive != false) {
			moveX -= 2;
			if (moveX < -2) moveX = -2;
		}
		break;
	case 'd':
		if (survive != false) {
			moveX += 1;
			if (moveX > 2) moveX -= 1;
		}
		break;
	case 'e':
		if (survive != false) {
			moveX += 2;
			if (moveX > 2) moveX = 2;
		}
		break;
	case 'r':
		if (menu != 0 || survive != false) {
			glLoadIdentity();
			PlaySound(NULL, 0, 0);
			gameStart();
			SCORE = 0;
			Start_on = true;
		}
		break;
	case 'l':
		if (menu != 0 || survive != false)
		{
			PlaySound(NULL, 0, 0);
			View = false;
			Viewfunc();
			menu = 2;
		}
		break;
	case '`':
		if (menu = 2) menu = 0;
		PlaySound(NULL, 0, 0);
		View = false;
		if(menu!=2) Viewfunc();
		menu = 0;
		survive = false;
		break;
	case 't':
		if (ForTest == false) ForTest = true;
		else if (ForTest == true) ForTest = false;
		break;

	}
	glutPostRedisplay();
}
void Mouse(int button, int state, int x, int y)
{

	x = x - 400;
	y = -y + 300;
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		Point.x = x;
		Point.z = y;
		printf("(%d,%d )", x, y);
		if (menu == 0)
		{
			if (First_menu)//첫번째 매뉴 시작,랭킹,나가기로 된다
			{
				if ((x<174 && x>-179) && (y<95 && y>-2))
				{
					First_menu = false;

				}
				else if ((x<174 && x>-179) && (y<-32 && y>-129))//메뉴 두번쨰
				{
					menu = 2;//랭킹 화면으로 
				}
				else if ((x<174 && x>-179) && (y<-150 && y>-247))//메뉴 세번쨰
				{
					glutLeaveMainLoop();
				}
			}
			else //난이도 선택
			{

				if ((x<174 && x>-179) && (y<95 && y>-2))
				{
					View = true;
					Viewfunc();

					if (menu == 0 && survive == false) {//메뉴 첫번쨰
						level = 1;
						gameStart();
						Start_on = true;
					}

				}
				else if ((x<174 && x>-179) && (y<-32 && y>-129))//메뉴 두번쨰
				{
					View = true;
					Viewfunc();
					if (menu == 0 && survive == false) {
						level = 2;
						gameStart();
						Start_on = true;
					}
				}
				else if ((x<174 && x>-179) && (y<-150 && y>-247))//메뉴 세번쨰
				{
					View = true;
					Viewfunc();
					if (menu == 0 && survive == false) {
						level = 3;
						gameStart();
						Start_on = true;
					}
				}
			}
		}
	}
}
void RoadTimer(int value) {
	srand(time(NULL));
	if (Start_count >= 30)
	{
		if (!Clear) {
			if (survive && menu == 1) {

				if (MoveRoad >= 28010 && MoveRoad < 32000) {
					RideWire = true;
				}
				else RideWire = false;

				if (!RideWire) {
					MoveRoad += 2 * SpeedTemp;
					if ((Count / NUM) > 0.5) MoveRoad += 1.5 * SpeedTemp;
				}

				else if (RideWire && RideWire_on) {
					MoveRoad += 4.95;
				}

			}
			if (Jump == 0) {
				collide();
			}
			if (Item_Homework)
				if (Life * 10 > Distance) {//커지면 거리가 좁혀짐
					Distance++;
					if (Life * 10 == Distance) Item_Homework = false;
				}
			if (Item_Class_Cancel)
				if (Life * 10 < Distance) {
					Distance--;
					if (Life * 10 == Distance) Item_Class_Cancel = false;
				}
			for (int i = 0; i < NUM; i++) {
				if (drawRoad[i][2].z + MoveRoad >= 100) {
					Count = i;
				}
			}

			if (Item_Fest) {
				for (int i = 300; i < 400; i++) {
					for (int j = 0; j < 5; j++) {
						road[i][j] = 7; // 축제용도로.
						road_on[i][j] = true;
					}
					if (i % 7 == 0) {
						for (int j = 0; j < 5; j++) {
							if (road[i][j] != 0) {
								int a = rand() % 3;
								if (a != 0) {
									road[i][j] = 7;   //축제도로
								}
								else {
									road[i][j] = 8;   //숙취음료
									road_on[i][j] = false;
								}
							}
						}
					}
				}
				Fest_bool = true;
				Item_Fest = false;
			}

			if (Fest_bool && survive) {
				if (drawRoad[399][2].z + MoveRoad >= 200) {
					if (drink <= 40) {
						Life++;
						if (Life * 10 > Distance) {
							Distance++;
						}
						if (Life == 4) survive = false;
					}
					Fest_bool = false;
				}
			}

			if (RideWire && survive) {
				if (!MoveWire_bool) {
					MoveWire += 2;
					if (MoveWire > 80) {
						MoveWire_bool = true;
					}
				}
				else {
					MoveWire -= 2;
					if (MoveWire < -80) {
						MoveWire_bool = false;
					}
				}

			}
			End();
		}
	}
	if (menu != 0 && Clear != true && survive == false)//게임이 끝났을때 //화면전환
	{

		if (endSpin_y >= 180)  Rank_Rect = true;

		else
		{
			endSpin_y += 2;
		}
		if (moveX == 2)
		{
			if (endTrans_x <= -50) {}
			else endTrans_x -= 10;
		}
		if (moveX == 1)
		{
			if (endTrans_x <= -25) {}
			else endTrans_x -= 10;
		}
		if (moveX == 0)
		{
			if (endTrans_x >= 20) {}
			else endTrans_x += 10;
		}

		if (moveX == -1) //왼
		{
			if (endTrans_x >= 25) {}
			else endTrans_x += 5;
		}
		if (moveX == -2)
		{
			if (endTrans_x >= 50) {}
			else endTrans_x += 10;
		}
		//

		if (endTrans_y >= 50) {}
		else endTrans_y += 10;
		if (endTrans_z <= -300) {}
		else endTrans_z -= 20;


	}
	glutPostRedisplay(); // 화면 재 출력
	glutTimerFunc(10, RoadTimer, 1); // 타이머함수 재 설정
}
void CharTimer(int value) {
	if (!Clear) {
		if (survive && menu == 1 && !RideWire) {
			if (moveX == 2) {
				if (charX < 80) charX++;
			}
			if (moveX == 1) {
				if (charX < 40) charX++;
				if (charX > 40) charX--;
			}
			if (moveX == 0) {
				if (charX < 0) charX++;
				if (charX > 0) charX--;
			}
			if (moveX == -1) {
				if (charX > -40) charX--;
				if (charX < -40) charX++;
			}
			if (moveX == -2) {
				if (charX > -80) charX--;
			}

			if (StudentTemp == 0) {
				StudentAngle += 0.2 * SpeedTemp;
				if (StudentAngle > 30) StudentTemp = 1;
			}
			else if (StudentTemp == 1) {
				StudentAngle -= 0.2 * SpeedTemp;
				if (StudentAngle < -30) StudentTemp = 0;
			}
		}
	}
	else {

	}
	glutPostRedisplay(); // 화면 재 출력
	glutTimerFunc(1, CharTimer, 1); // 타이머함수 재 설정
}
void MenuTimer(int value) {
	if (menu == 0) {

	}
	glutPostRedisplay(); // 화면 재 출력
	glutTimerFunc(1, MenuTimer, 1); // 타이머함수 재 설정
}
void JumpTimer(int value) {
	if (!Clear) {
		if (survive) {
			if (Jump_on) {
				if (Jump_bool == false)
				{
					Jump += 0.9 * SpeedTemp;
					if (Jump > 25)
					{
						Jump = 25;
						Jump_bool = true;
					}

				}
				if (Jump_bool) {
					Jump -= 0.9 * SpeedTemp;
					if (Jump < 0)
					{
						Jump = 0;
						Jump_bool = false;
						Jump_on = false;
					}

				}
			}
			else if (Item_Run) {
				if (Jump_bool == false)
				{
					Jump += 0.2 * SpeedTemp;
					MoveRoad += 2 * SpeedTemp;
					Fest_Student_Angle = -(Jump / 30) * 90;
					if (Jump > 30)
					{
						Jump = 30;
						Jump_bool = true;
					}

				}
				if (Jump_bool) {
					Jump -= 0.2 * SpeedTemp;
					MoveRoad += 2 * SpeedTemp;
					Fest_Student_Angle = -(30 / Jump) * 90;
					if (Jump < 0)
					{
						Jump = 0;
						Fest_Student_Angle = 0;
						Jump_bool = false;
						Item_Run = false;
					}

				}
			}
			else if (RideWire_on) {
				if (!Jump_bool)
				{
					Jump += 1.5;
					if (Jump > 60)
					{
						Jump = 60;
						Jump_bool = true;
					}

				}
				if (Jump_bool) {
					Jump -= 1.5;
					if (Jump < 0)
					{
						Jump = 0;
						Jump_bool = false;
						RideWire_on = false;
					}

				}
			}
		}
	}
	else {
		if (Jump_bool == false)
		{
			Jump += 1;
			if (Jump > 25)
			{
				Jump = 25;
				Jump_bool = true;
			}

		}
		if (Jump_bool) {
			Jump -= 1;
			if (Jump < 0)
			{
				Jump = 0;
				Jump_bool = false;
				Jump_on = false;
			}

		}
	}
	glutPostRedisplay(); // 화면 재 출력
	glutTimerFunc(10, JumpTimer, 1); // 타이머함수 재 설정
}
void TimeTimer(int value)
{
	int Temp;
	if (Start_on)
	{
		Start_count += 1;
		if (Start_count > 30)
		{

			Start_on = false;
		}
	}
	if (menu != 0 && Clear != true && survive != false && Start_count >= 30)
	{
		int num = 0;
		SCORE += 12;
		if (SCORE > 999999) SCORE = 999999;//점수 최대치
		Temp = SCORE;
		while (Temp != 0)//자리수 나누기
		{
			Text_score_S[num] = Temp % 10;
			Temp /= 10;
			num += 1;
		}


		Rank_Score[rank_num] = SCORE;
		printf("%d\n", SCORE); // 콘솔에 점수 표시
	}
	if (menu == 1 && survive == false)//게임이 끝났을때
	{
		if (Rank_bool)//한번만 실행한다.
		{
			
			if (Clear)
			{
				PlaySound(NULL, 0, 0);
				PlaySound(TEXT(SOUND_FILE_BGM3), NULL, SND_FILENAME | SND_ASYNC | SND_LOOP | SND_NODEFAULT | SND_NOSTOP);
			}
			else
			{
				PlaySound(NULL, 0, 0);
				PlaySound(TEXT(SOUND_FILE_BGM2), NULL, SND_FILENAME | SND_ASYNC | SND_LOOP | SND_NODEFAULT | SND_NOSTOP);
			}
			
			Rank_Func();
			Rank_Score[rank_num] = SCORE;
			Rank_grade[rank_num] = RANK;
			rank_num += 1;
			FileOutput();
			Rank_bool = false;

			//menu = 2;
		}
	}
	glutPostRedisplay(); // 화면 재 출력
	glutTimerFunc(100, TimeTimer, 1); // 타이머함수 재 설정
}
//충돌체크
int inRect(int x, int y, int rectx1, int recty1, int rectx2, int recty2) {
	if ((x >= rectx1 && x <= rectx2) && (y >= recty1 && y <= recty2))
		return 1;
	else return 0;
}

void collide() {
	double range = 10; // 충돌범위
	for (int i = 0; i < NUM; i++) {
		for (int j = 0; j < 5; j++) {
			if (!RideWire) {
				if (inRect(charX - range, 100 - range, drawRoad[i][j].x - range, drawRoad[i][j].z + MoveRoad - range, drawRoad[i][j].x + range, drawRoad[i][j].z + MoveRoad + range) ||
					inRect(charX - range, 100 + range, drawRoad[i][j].x - range, drawRoad[i][j].z + MoveRoad - range, drawRoad[i][j].x + range, drawRoad[i][j].z + MoveRoad + range) ||
					inRect(charX + range, 100 - range, drawRoad[i][j].x - range, drawRoad[i][j].z + MoveRoad - range, drawRoad[i][j].x + range, drawRoad[i][j].z + MoveRoad + range) ||
					inRect(charX + range, 100 + range, drawRoad[i][j].x - range, drawRoad[i][j].z + MoveRoad - range, drawRoad[i][j].x + range, drawRoad[i][j].z + MoveRoad + range)) {
					//충돌시

					if (road[i][j] == 0 && !ForTest) {
						survive = false;
					}
					if (road[i][j] == 1) {//흰색타일 충돌체크
						if (road_on[i][j]) {
							SCORE += 100;
							road_on[i][j] = false;
						}
					}
					if (road[i][j] == 2) {
						SCORE += 3000;
						Item_Run = true;
					}
					if (road[i][j] == 3) {
						Item_Homework = true;
						road[i][j] = 1;
						SCORE -= 2000;
						Life++;
						if (Life == 4) survive = false;
					}
					if (road[i][j] == 4) {
						Item_Class_Cancel = true;
						road[i][j] = 1;
						SCORE += 5000;
						if (Life > 0)   Life--;
					}
					if (road[i][j] == 6) {
						SCORE += 10000;
						Item_Fest = true;
					}
					if (road[i][j] == 7) {//축제타일 충돌체크
						if (road_on[i][j]) {
							SCORE += 200;
							road_on[i][j] = false;
						}
					}
					if (road[i][j] == 8) {
						drink += 4;
						SCORE += 1000;
						road[i][j] = 7;
					}
					if (road[i][2] == 10) {
						charX = drawRoad[i][2].x;
						charZ = drawRoad[i][2].z + MoveRoad - 95;
						RideWire = true;
					}
				}
			}
			else {
				range = 10;
				if (inRect(charX - range, 100 - range, drawRoad[i][j].x - range + MoveWire, drawRoad[i][j].z + MoveRoad - range, drawRoad[i][j].x + range + MoveWire, drawRoad[i][j].z + MoveRoad + range) ||
					inRect(charX - range, 100 + range, drawRoad[i][j].x - range + MoveWire, drawRoad[i][j].z + MoveRoad - range, drawRoad[i][j].x + range + MoveWire, drawRoad[i][j].z + MoveRoad + range) ||
					inRect(charX + range, 100 - range, drawRoad[i][j].x - range + MoveWire, drawRoad[i][j].z + MoveRoad - range, drawRoad[i][j].x + range + MoveWire, drawRoad[i][j].z + MoveRoad + range) ||
					inRect(charX + range, 100 + range, drawRoad[i][j].x - range + MoveWire, drawRoad[i][j].z + MoveRoad - range, drawRoad[i][j].x + range + MoveWire, drawRoad[i][j].z + MoveRoad + range)) {
					//충돌시
					if (road[i][j] == 10) {
						charX = MoveWire;
						charZ = drawRoad[i][2].z + MoveRoad - 95;
						SCORE -= 10;
					}
					else if (road[i][j] == 9) survive = false;

					if (road[i][j] == 11) {
						RideWire = false;
					}

				}
			}
		}
	}
}

void End() {
	double range = 5; // 충돌범위
	for (int j = 0; j < 5; j++) {
		if (inRect(charX - range, 100 - range, drawRoad[NUM - 1][j].x - range, drawRoad[NUM - 1][j].z + MoveRoad - range, drawRoad[NUM - 1][j].x + range, drawRoad[NUM - 1][j].z + MoveRoad + range) ||
			inRect(charX - range, 100 + range, drawRoad[NUM - 1][j].x - range, drawRoad[NUM - 1][j].z + MoveRoad - range, drawRoad[NUM - 1][j].x + range, drawRoad[NUM - 1][j].z + MoveRoad + range) ||
			inRect(charX + range, 100 - range, drawRoad[NUM - 1][j].x - range, drawRoad[NUM - 1][j].z + MoveRoad - range, drawRoad[NUM - 1][j].x + range, drawRoad[NUM - 1][j].z + MoveRoad + range) ||
			inRect(charX + range, 100 + range, drawRoad[NUM - 1][j].x - range, drawRoad[NUM - 1][j].z + MoveRoad - range, drawRoad[NUM - 1][j].x + range, drawRoad[NUM - 1][j].z + MoveRoad + range)) {
			//충돌시
			Clear = true;
			survive = false;
		}
	}
}
//시작시 초기화할것들
void gameStart() {
	menu = 1;
	MoveRoad = 0;
	moveX = 0;
	MoveWire = 0;
	SpeedTemp = level;
	Jump = 0;
	Life = 0;
	Distance = 0;
	drink = 0;
	Item_Run = false;
	Jump_on = false;
	Jump_bool = false;
	survive = true;
	ForTest = false;
	Clear = false;
	Rank_bool = true;
	endSpin_y = 0;
	endTrans_x = 0;
	endTrans_y = 0;
	endTrans_z = 0;
	Rank_Rect = false;
	RANK = 0;
	SCORE = 0;
	//Start_count = 0;// 나중에 킨다
	Start_count = 40;
	Start_on = false;
	PlaySound(TEXT(SOUND_FILE_BGM), NULL, SND_FILENAME | SND_ASYNC | SND_LOOP | SND_NODEFAULT | SND_NOSTOP);
	RandomRoad();
	First_menu = true;
	for (int i = 0; i < 6; i++)   Text_score_S[i] = 0;
}

void Make_menu() {
	glPushMatrix();
	glColor3f(1.0, 1.0, 0.0);
	glScalef(1.8, 1.8, 2);
	glTranslatef(0, 00, 0);
	//glBegin(GL_QUADS);   //바닥 //가로 360, 세로 300 
	//glVertex2f(-180, 150);
	//glVertex2f(180, 150);
	//glVertex2f(180, -150);
	//glVertex2f(-180, -150);
	//glEnd();

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[2]);
	if (First_menu)
	{
		glPushMatrix();
		glBindTexture(GL_TEXTURE_2D, textures[4]);
		glColor3f(1.0, 1.0, 1.0);
		glTranslatef(0, 0, 10);
		glTranslatef(0, 100, 0);
		glBegin(GL_QUADS);   // 이름판 //가로 300, 세로 60 이름판
		glTexCoord2f(0, 1);
		glVertex2f(-150, 30);
		glTexCoord2f(1, 1);
		glVertex2f(150, 30);
		glTexCoord2f(1, 0);
		glVertex2f(150, -30);
		glTexCoord2f(0, 0);
		glVertex2f(-150, -30);
		glEnd();
		glPopMatrix();
		glBindTexture(GL_TEXTURE_2D, textures[2]);
		glPushMatrix();
		glColor3f(1, 1, 1);
		glTranslatef(0, 25, 10);
		glBegin(GL_QUADS);   //시작  //가로 200, 세로 60 선택지
		glTexCoord2f(0, 1);
		glVertex2f(-100, 30);
		glTexCoord2f(0.74, 1);
		glVertex2f(100, 30);
		glTexCoord2f(0.74, 0.85);
		glVertex2f(100, -30);
		glTexCoord2f(0, 0.85);
		glVertex2f(-100, -30);
		glEnd();
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0, 0, 10);
		glTranslatef(0, -45, 0);
		glBegin(GL_QUADS);   // 랭킹 //가로 200, 세로 60 선택지
		glTexCoord2f(0, 0.85);
		glVertex2f(-100, 30);
		glTexCoord2f(0.64, 0.85);
		glVertex2f(100, 30);
		glTexCoord2f(0.64, 0.66);
		glVertex2f(100, -30);
		glTexCoord2f(0, 0.66);
		glVertex2f(-100, -30);
		glEnd();
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0, 0, 10);
		glTranslatef(0, -110, 0);
		glBegin(GL_QUADS);   // 종료 //가로 200, 세로 60 선택지
		glTexCoord2f(0, 0.66);
		glVertex2f(-100, 30);
		glTexCoord2f(0.6, 0.66);
		glVertex2f(100, 30);
		glTexCoord2f(0.6, 0.51);
		glVertex2f(100, -30);
		glTexCoord2f(0, 0.51);
		glVertex2f(-100, -30);
		glEnd();
		glPopMatrix();
	}
	else
	{
		glPushMatrix();
		glColor3f(1.0, 0.0, 0.0);
		glTranslatef(0, 0, 10);
		glTranslatef(0, 100, 0);
		glBegin(GL_QUADS);   // 학점 선택 //가로 300, 세로 60 이름판
		glTexCoord2f(0, 0.5);
		glVertex2f(-150, 30);
		glTexCoord2f(0.93, 0.5);
		glVertex2f(150, 30);
		glTexCoord2f(0.93, 0.4);
		glVertex2f(150, -30);
		glTexCoord2f(0, 0.4);
		glVertex2f(-150, -30);
		glEnd();
		glPopMatrix();

		glPushMatrix();
		glColor3f(1, 1, 1);
		glTranslatef(0, 25, 10);
		glBegin(GL_QUADS);   //18  //가로 200, 세로 60 선택지
		glTexCoord2f(0, 0.38);
		glVertex2f(-100, 30);
		glTexCoord2f(0.2, 0.38);
		glVertex2f(100, 30);
		glTexCoord2f(0.2, 0.28);
		glVertex2f(100, -30);
		glTexCoord2f(0, 0.28);
		glVertex2f(-100, -30);
		glEnd();
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0, 0, 10);
		glTranslatef(0, -45, 0);
		glBegin(GL_QUADS);   // 21 //가로 200, 세로 60 선택지
		glTexCoord2f(0.25, 0.38);
		glVertex2f(-100, 30);
		glTexCoord2f(0.45, 0.38);
		glVertex2f(100, 30);
		glTexCoord2f(0.45, 0.28);
		glVertex2f(100, -30);
		glTexCoord2f(0.25, 0.28);
		glVertex2f(-100, -30);
		glEnd();
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0, 0, 10);
		glTranslatef(0, -110, 0);
		glBegin(GL_QUADS);   // 24 //가로 200, 세로 60 선택지
		glTexCoord2f(0.5, 0.38);
		glVertex2f(-100, 30);
		glTexCoord2f(0.8, 0.38);
		glVertex2f(100, 30);
		glTexCoord2f(0.8, 0.28);
		glVertex2f(100, -30);
		glTexCoord2f(0.5, 0.28);
		glVertex2f(-100, -30);
		glEnd();
		glPopMatrix();
	}
	glDisable(GL_TEXTURE_2D);

	glPopMatrix();

}
//진행률
void Progress() {
	glPushMatrix();
	glTranslatef(0, 150, 0);
	glColor3f(1.0, 1.0, 1.0);
	glScalef(1.0, 0.05, 0.0001);
	glutSolidCube(400);
	glPopMatrix();


	// 움직이는 바
	glPushMatrix();
	glTranslatef(-200 + (Count / NUM) * 400, 149.5, 1);
	glColor3f(0.0, 1.0, 0.0);
	glPushMatrix();
	glTranslatef(20, 45, 0);
	glScalef(0.2, 0.5, 0.1);
	glRotatef(-90, 0, 1, 0);
	glTranslatef(0, 0, 0);//학생의 위치를 바꿔준다.
	Student();
	Teacher();
	glPopMatrix();
	glScalef(0.02, 0.05, 0.0001);
	glutSolidCube(400);
	glPopMatrix();
}
void DrunkenBar() {
	glPushMatrix();
	glTranslatef(0, 120, 0);
	glColor3f(1.0, 1.0, 1.0);
	glScalef(1.0, 0.05, 0.0001);
	glutSolidCube(400);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(60, 119.5, 1);
	glColor3f(0.0, 1.0, 1.0);
	glScalef(0.02, 0.05, 0.0001);
	glutSolidCube(400);
	glPopMatrix();

	// 움직이는 바
	glPushMatrix();
	glTranslatef(-200 + (Count - 300 - drink) * 4, 119.5, 1);
	glColor3f(0.0, 1.0, 0.0);
	glScalef(0.02, 0.05, 0.0001);
	glutSolidCube(400);
	glPopMatrix();
}
//길설정
void Roadsetting() {
	if (set == 0) {
		int tempZ = 70;
		for (int i = 0; i < NUM; i++) {
			int tempX = -TileInterval * 2;
			for (int j = 0; j < 5; j++) {
				drawRoad[i][j].x = tempX;
				drawRoad[i][j].z = tempZ;
				tempX += TileInterval;
			}
			tempZ -= TileInterval - 2;
		}
		set = 1;
	}

}

//장애물,아이템 랜덤설정

void RandomRoad() {
	srand(time(NULL));
	for (int i = 0; i < NUM; i++) {
		for (int j = 0; j < 5; j++) {
			road[i][j] = 1;
			road_on[i][j] = true; // 흰색 타일은 참
		}

		if (i > 15 && (i + 4) % 16 == 0 && i != 0) {
			int j = rand() % 5;
			int Item = rand() % 10;
			if (Item < 4) {
				road[i][j] = 2;
				road_on[i][j] = false;//나머지 타일은 거짓
			}
			else if (Item >= 4 && Item < 8) {
				road[i][j] = 3;
				road_on[i][j] = false;
			}
			else if (Item >= 8 && Item < 10) {
				road[i][j] = 4;
				road_on[i][j] = false;
			}
		}
		if (i > 15 && i % 8 == 0 && i != 0) {
			int count = 0; // 장애물 개수 0으로 초기화
			while (count < level + 1) {
				for (int j = 0; j < 5; j++) {
					if (road[i][j] != 0) {
						int a = rand() % 2;
						if (a == 1) {
							road[i][j] = 0;
							count++;
							if (count == level + 1) break;
						}
					}
				}
			}
		}
		//축제구간 설정
		if (i == 300) for (int j = 0; j < 5; j++) {
			if (j != 2)
			{
				road[i][j] = 6;

			}
			else road[i][j] = 1;
		}
		if (i == 400)  for (int j = 0; j < 5; j++) {
			road[i][j] = 1;
		}
		//줄타기구간
		if (i > 650 && i < 699) {
			for (int j = 0; j < 5; j++) {
				if (road[i][j] != 0 && road[i][j] != 1) {
					road[i][j] = 1;
				}
			}
		}

		if (i >= 700 && i < 800) {

			for (int j = 0; j < 5; j++) {
				road[i][j] = 0;
			}

			road[i][4] = 9;
			road[i][0] = 9;

			if (i % 10 == 0) {
				road[i][2] = 10;
			}

		}

		if (i == 800) {
			for (int j = 0; j < 5; j++)
				road[i][j] = 11;
		}

		if (i > 950 && i < 999) {
			for (int j = 0; j < 5; j++) {
				if (road[i][j] != 0 && road[i][j] != 1) {
					road[i][j] = 1;
				}
			}
		}

		//엔딩 설정
		for (int j = 0; j < 5; j++) road[NUM - 1][j] = 5;

	}
}

//길 그리기
void Road() {
	glPushMatrix();
	for (int i = 0; i < NUM; i++) {
		if (drawRoad[i][2].z + MoveRoad < 200 && drawRoad[i][2].z + MoveRoad > -1000) {

			//중간고사 패스구간
			if (i == 500) {
				glPushMatrix();

				glEnable(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D, textures[16]);
				glTranslatef(120, -100, drawRoad[i][0].z + MoveRoad);
				glRotatef(30, 1, 0, 0);
				glRotatef(-30, 0, 1, 0);
				glTranslatef(0, 10, 0);
				glBegin(GL_QUADS);
				glColor3f(1, 1, 1);
				glTexCoord2f(0, 0);
				glVertex3f(-20, 0, 20);
				glTexCoord2f(1, 0);
				glVertex3f(20, 0, 20);
				glTexCoord2f(1, 1);
				glVertex3f(20, 0, -20);
				glTexCoord2f(0, 1);
				glVertex3f(-20, 0, -20);
				glEnd();
				glDisable(GL_TEXTURE_2D);
				glPopMatrix();

				glPushMatrix();
				glEnable(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D, textures[16]);
				glTranslatef(-120, -100, drawRoad[i][0].z + MoveRoad);
				glRotatef(30, 1, 0, 0);
				glRotatef(30, 0, 1, 0);
				glTranslatef(0, 10, 0);
				glBegin(GL_QUADS);
				glColor3f(1, 1, 1);
				glTexCoord2f(0, 0);
				glVertex3f(-20, 0, 20);
				glTexCoord2f(1, 0);
				glVertex3f(20, 0, 20);
				glTexCoord2f(1, 1);
				glVertex3f(20, 0, -20);
				glTexCoord2f(0, 1);
				glVertex3f(-20, 0, -20);
				glEnd();
				glDisable(GL_TEXTURE_2D);
				glPopMatrix();
			}
			for (int j = 0; j < 5; j++) {
				// 일반 길
				if (road[i][j] == 1) {
					glPushMatrix();
					glEnable(GL_TEXTURE_2D);
					glBindTexture(GL_TEXTURE_2D, textures[8]);
					glTranslatef(drawRoad[i][j].x, -100, drawRoad[i][j].z + MoveRoad);
					glBegin(GL_QUADS);
					glColor3f(1, 1, 1);
					glTexCoord2f(0, 0);
					glVertex3f(-20, 0, 20);
					glTexCoord2f(0, 1);
					glVertex3f(20, 0, 20);
					glTexCoord2f(1, 1);
					glVertex3f(20, 0, -20);
					glTexCoord2f(1, 0);
					glVertex3f(-20, 0, -20);
					glEnd();
					glDisable(GL_TEXTURE_2D);
					glPopMatrix();
				}
				// 노란길 - 출튀
				if (road[i][j] == 2) {
					glPushMatrix();
					glEnable(GL_TEXTURE_2D);
					glBindTexture(GL_TEXTURE_2D, textures[9]);
					glTranslatef(drawRoad[i][j].x, -100, drawRoad[i][j].z + MoveRoad);
					glRotatef(30, 1, 0, 0);
					glTranslatef(0, 10, 0);
					glBegin(GL_QUADS);
					glColor3f(1, 1, 1);
					glTexCoord2f(0, 0);
					glVertex3f(-20, 0, 20);
					glTexCoord2f(1, 0);
					glVertex3f(20, 0, 20);
					glTexCoord2f(1, 1);
					glVertex3f(20, 0, -20);
					glTexCoord2f(0, 1);
					glVertex3f(-20, 0, -20);
					glEnd();
					glDisable(GL_TEXTURE_2D);
					glPopMatrix();
				}
				// 빨간길 - 과제
				if (road[i][j] == 3) {
					glPushMatrix();
					glEnable(GL_TEXTURE_2D);
					glBindTexture(GL_TEXTURE_2D, textures[10]);
					glTranslatef(drawRoad[i][j].x, -100, drawRoad[i][j].z + MoveRoad);
					glRotatef(30, 1, 0, 0);
					glTranslatef(0, 10, 0);
					glBegin(GL_QUADS);
					glColor3f(1, 1, 1);
					glTexCoord2f(0, 0);
					glVertex3f(-20, 0, 20);
					glTexCoord2f(1, 0);
					glVertex3f(20, 0, 20);
					glTexCoord2f(1, 1);
					glVertex3f(20, 0, -20);
					glTexCoord2f(0, 1);
					glVertex3f(-20, 0, -20);
					glEnd();
					glDisable(GL_TEXTURE_2D);
					glPopMatrix();
				}
				// 파란길 - 휴강
				if (road[i][j] == 4) {
					glPushMatrix();
					glEnable(GL_TEXTURE_2D);
					glBindTexture(GL_TEXTURE_2D, textures[11]);
					glTranslatef(drawRoad[i][j].x, -100, drawRoad[i][j].z + MoveRoad);
					glRotatef(30, 1, 0, 0);
					glTranslatef(0, 10, 0);
					glBegin(GL_QUADS);
					glColor3f(1, 1, 1);
					glTexCoord2f(0, 0);
					glVertex3f(-20, 0, 20);
					glTexCoord2f(1, 0);
					glVertex3f(20, 0, 20);
					glTexCoord2f(1, 1);
					glVertex3f(20, 0, -20);
					glTexCoord2f(0, 1);
					glVertex3f(-20, 0, -20);
					glEnd();
					glDisable(GL_TEXTURE_2D);
					glPopMatrix();
				}
				//끝
				if (road[i][j] == 5) {
					glPushMatrix();
					glEnable(GL_TEXTURE_2D);
					glBindTexture(GL_TEXTURE_2D, textures[14]);
					glTranslatef(drawRoad[i][j].x, -100, drawRoad[i][j].z + MoveRoad);
					glRotatef(30, 1, 0, 0);
					glTranslatef(0, 10, 0);
					glBegin(GL_QUADS);
					glColor3f(1, 1, 1);
					glTexCoord2f(0, 0);
					glVertex3f(-20, 0, 20);
					glTexCoord2f(1, 0);
					glVertex3f(20, 0, 20);
					glTexCoord2f(1, 1);
					glVertex3f(20, 0, -20);
					glTexCoord2f(0, 1);
					glVertex3f(-20, 0, -20);
					glEnd();
					glDisable(GL_TEXTURE_2D);
					glPopMatrix();
				}
				//축제
				if (road[i][j] == 6 || road[i][j] == 7) {
					glPushMatrix();
					glEnable(GL_TEXTURE_2D);
					glBindTexture(GL_TEXTURE_2D, textures[12]);
					glTranslatef(drawRoad[i][j].x, -100, drawRoad[i][j].z + MoveRoad);
					if (road[i][j] == 6)   glRotatef(30, 1, 0, 0);
					glTranslatef(0, 10, 0);
					glBegin(GL_QUADS);
					glColor3f(1, 1, 1);
					glTexCoord2f(0, 0);
					glVertex3f(-20, 0, 20);
					glTexCoord2f(1, 0);
					glVertex3f(20, 0, 20);
					glTexCoord2f(1, 1);
					glVertex3f(20, 0, -20);
					glTexCoord2f(0, 1);
					glVertex3f(-20, 0, -20);
					glEnd();
					glDisable(GL_TEXTURE_2D);
					glPopMatrix();
				}
				if (road[i][j] == 8) {
					glPushMatrix();
					glEnable(GL_TEXTURE_2D);
					glBindTexture(GL_TEXTURE_2D, textures[13]);
					glTranslatef(drawRoad[i][j].x, -100, drawRoad[i][j].z + MoveRoad);
					glRotatef(30, 1, 0, 0);
					glTranslatef(0, 10, 0);
					glBegin(GL_QUADS);
					glColor3f(1, 1, 1);
					glTexCoord2f(0, 0);
					glVertex3f(-20, 0, 20);
					glTexCoord2f(1, 0);
					glVertex3f(20, 0, 20);
					glTexCoord2f(1, 1);
					glVertex3f(20, 0, -20);
					glTexCoord2f(0, 1);
					glVertex3f(-20, 0, -20);
					glEnd();
					glDisable(GL_TEXTURE_2D);
					glPopMatrix();
				}

				if (road[i][j] == 10) {
					if (charZ == drawRoad[i][2].z + MoveRoad - 95) {
						glPushMatrix();

						glColor3f(0.5f, 1.0f, 0.5f);
						glLineWidth(3.0);
						glBegin(GL_LINES);
						glVertex3f(drawRoad[i][j].x, 100, drawRoad[i][j].z + MoveRoad);
						glVertex3f(drawRoad[i][j].x + MoveWire, -70, drawRoad[i][j].z + MoveRoad);
						glEnd();


						glPopMatrix();
					}
					else {
						glPushMatrix();
						glColor3f(0.0f, 0.7f, 0.6f);
						glLineWidth(3.0);
						glBegin(GL_LINES);
						glVertex3f(drawRoad[i][j].x, 100, drawRoad[i][j].z + MoveRoad);
						glVertex3f(drawRoad[i][j].x, -70, drawRoad[i][j].z + MoveRoad);
						glEnd();

						glPopMatrix();
					}
				}
				if (road[i][j] == 11) {
					glPushMatrix();

					glTranslatef(drawRoad[i][j].x, -100, drawRoad[i][j].z + MoveRoad);
					glColor3f(1.0f, 0.7f, 0.6f);
					glScalef(1.0, 0.01, 1.0);
					glutSolidCube(40);

					glPopMatrix();
				}
			}
		}
	}
	glPopMatrix();
}

//캐릭터 그리기
void Student() {
	glEnable(GL_TEXTURE_2D);
	glPushMatrix();
	glTranslatef(0, -80, 100);//학생의 초기위치
	glTranslatef(charX, Jump, charZ);//학생의 위치를 바꿔준다.
	glScalef(3, 3, 3);   //학생 확대 (기본 3,3,3)
	glRotatef(90, 0, 1, 0);//학생이 가는 방향을 바라보게
						   //다리1
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, textures[7]);
	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glRotatef(StudentAngle, 0, 0, 1);
	glTranslatef(0, -3, 0);
	glTranslatef(0, 0, 1.5);
	glScalef(0.3, 1.0, 0.3);
	glColor3f(1.0f, 1.0f, 1.0f);
	glutSolidCube(4);
	glPopMatrix();
	glPushMatrix();
	// 다리2
	glRotatef(-StudentAngle, 0, 0, 1);
	glTranslatef(0, -3, 0);
	glTranslatef(0, 0, -1.5);
	glScalef(0.3, 1.0, 0.3);
	glColor3f(1.0f, 1.0f, 1.0f);
	glutSolidCube(4);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	//몸통
	glPushMatrix();

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[6]);
	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glColor3f(1.0f, 1.0f, 1.0f);
	glScalef(0.8, 1.0, 1);
	glutSolidCube(5);
	glPopMatrix();

	// 팔1
	glTranslatef(0, 3, 0);
	glPushMatrix();
	//윗팔
	glRotatef(-StudentAngle, 0, 0, 1);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[6]);
	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glTranslatef(0, -2, 0);
	glTranslatef(0, 0, 3.5);
	glScalef(0.3, 0.5, 0.3);
	glColor3f(1.0f, 1.0f, 1.0f);
	glutSolidCube(4);

	glDisable(GL_TEXTURE_2D);
	//아랫팔
	glTranslatef(0, -4, 0);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[5]);
	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glutSolidCube(4);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();

	glPushMatrix();
	// 팔2
	//윗팔

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[6]);
	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glRotatef(StudentAngle, 0, 0, 1);
	glTranslatef(0, -2, 0);
	glTranslatef(0, 0, -3.5);
	glScalef(0.3, 0.5, 0.3);
	glColor3f(1.0f, 1.0f, 1.0f);
	glutSolidCube(4);
	glDisable(GL_TEXTURE_2D);
	//아랫팔

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[5]);
	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glTranslatef(0, -4, 0);
	glutSolidCube(4);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();

	// 머리
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[5]);
	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glTranslatef(0, 1, 0);
	glColor3f(1.0f, 1.0f, 1.0f);
	glutSolidCube(3);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	//코
	glPushMatrix();
	glTranslatef(2, 1, 0);
	glScalef(0.3, 0.3, 0.3);
	glColor3f(1.0f, 0, 0);
	glutSolidCube(2);
	glPopMatrix();
	glPopMatrix();
}

//선생님 그리기
void Teacher() {
	glPushMatrix();
	glTranslatef(0, -80, 180 - Distance);//선생님의 초기위치
	glScalef(3, 3, 3);   //선생님 확대 (기본 3,3,3)
	glRotatef(90, 0, 1, 0);//선생님이 가는 방향을 바라보게
						   //다리1
	glPushMatrix();
	glRotatef(StudentAngle, 0, 0, 1);
	glTranslatef(0, -3, 0);
	glTranslatef(0, 0, 1.5);
	glScalef(0.3, 1.0, 0.3);
	glColor3f(0.1f, 0.1f, 0.1f);
	glutSolidCube(4);
	glPopMatrix();
	glPushMatrix();
	// 다리2
	glRotatef(-StudentAngle, 0, 0, 1);
	glTranslatef(0, -3, 0);
	glTranslatef(0, 0, -1.5);
	glScalef(0.3, 1.0, 0.3);
	glColor3f(0.1f, 0.1f, 0.1f);
	glutSolidCube(4);
	glPopMatrix();
	//몸통
	glPushMatrix();
	glColor3f(0.1f, 0.1f, 0.1f);
	glScalef(0.8, 1.0, 1);
	glutSolidCube(5);
	glPopMatrix();

	// 팔1
	glTranslatef(0, 3, 0);
	glPushMatrix();
	//윗팔
	glRotatef(-StudentAngle, 0, 0, 1);
	glTranslatef(0, -2, 0);
	glTranslatef(0, 0, 3.5);
	glScalef(0.3, 0.5, 0.3);
	glColor3f(0.1f, 0.1f, 0.1f);
	glutSolidCube(4);
	//아랫팔
	glTranslatef(0, -4, 0);
	glutSolidCube(4);
	glPopMatrix();

	glPushMatrix();
	// 팔2
	//윗팔
	glRotatef(StudentAngle, 0, 0, 1);
	glTranslatef(0, -2, 0);
	glTranslatef(0, 0, -3.5);
	glScalef(0.3, 0.5, 0.3);
	glColor3f(0.1f, 0.1f, 0.1f);
	glutSolidCube(4);
	//아랫팔
	glTranslatef(0, -4, 0);
	glutSolidCube(4);
	glPopMatrix();

	// 머리
	glPushMatrix();
	glTranslatef(0, 1, 0);
	glRotatef(StudentAngle / 4, 1, 0, 0);
	glColor3f(0.1f, 0.1f, 0.1f);
	glutSolidCube(3);
	glPopMatrix();
	//코
	glPushMatrix();
	glTranslatef(2, 1, 0);
	glScalef(0.3, 0.3, 0.3);
	glColor3f(0.5f, 0.1f, 0.1f);
	glutSolidCube(1);
	glPopMatrix();
	glPopMatrix();
}

// 날라가는 학생
void Fest_Student() {
	glEnable(GL_TEXTURE_2D);
	glPushMatrix();
	glTranslatef(0, -80, 100);//학생의 초기위치
	glTranslatef(charX, Jump, 0);//학생의 위치를 바꿔준다.
	glScalef(3, 3, 3);   //학생 확대 (기본 3,3,3)
	glRotatef(90, 0, 1, 0);//학생이 가는 방향을 바라보게
	glRotatef(Fest_Student_Angle, 0, 0, 1);   // 학생 날아가는 각도
											  //다리1
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, textures[7]);
	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glTranslatef(0, -3, 0);
	glTranslatef(0, 0, 1.5);
	glScalef(0.3, 1.0, 0.3);
	glColor3f(1.0f, 1.0f, 1.0f);
	glutSolidCube(4);

	glPopMatrix();
	glPushMatrix();
	// 다리2
	glTranslatef(0, -3, 0);
	glTranslatef(0, 0, -1.5);
	glScalef(0.3, 1.0, 0.3);
	glColor3f(1.0f, 1.0f, 1.0f);
	glutSolidCube(4);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	//몸통
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[6]);
	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glColor3f(1.0f, 1.0f, 1.0f);
	glScalef(0.8, 1.0, 1);
	glutSolidCube(5);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();

	// 팔1
	glTranslatef(0, 3, 0);
	glPushMatrix();
	//윗팔
	glRotatef(-180, 0, 0, 1);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[6]);
	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glTranslatef(0, 0, 3.5);
	glScalef(0.3, 0.5, 0.3);
	glColor3f(1.0f, 1.0f, 1.0f);
	glutSolidCube(4);
	glDisable(GL_TEXTURE_2D);
	//아랫팔
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[5]);
	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glTranslatef(0, -4, 0);
	glutSolidCube(4);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();

	glPushMatrix();
	// 팔2
	//윗팔
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[6]);
	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glRotatef(-180, 0, 0, 1);
	glTranslatef(0, 0, -3.5);
	glScalef(0.3, 0.5, 0.3);
	glColor3f(1.0f, 1.0f, 1.0f);
	glutSolidCube(4);
	glDisable(GL_TEXTURE_2D);
	//아랫팔
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[5]);
	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glTranslatef(0, -4, 0);
	glutSolidCube(4);

	glDisable(GL_TEXTURE_2D);
	glPopMatrix();

	// 머리
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[5]);
	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glTranslatef(0, 1, 0);
	glColor3f(1.0f, 1.0f, 1.0f);
	glutSolidCube(3);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	//코
	glPushMatrix();
	glTranslatef(2, 1, 0);
	glScalef(0.3, 0.3, 0.3);
	glColor3f(1.0f, 0, 0);
	glutSolidCube(2);
	glPopMatrix();
	glPopMatrix();
}

void Wire_Student() {
	glEnable(GL_TEXTURE_2D);
	glPushMatrix();
	glTranslatef(0, -80, 100);//학생의 초기위치
	glTranslatef(charX, Jump, 0);//학생의 위치를 바꿔준다.
	glScalef(3, 3, 3);   //학생 확대 (기본 3,3,3)
	glRotatef(90, 0, 1, 0);//학생이 가는 방향을 바라보게
						   //다리1
	glPushMatrix();

	glBindTexture(GL_TEXTURE_2D, textures[7]);
	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glTranslatef(0, -3, 0);
	glTranslatef(0, 0, 1.5);
	glScalef(0.3, 1.0, 0.3);
	glColor3f(1.0f, 1.0f, 1.0f);
	glutSolidCube(4);
	glPopMatrix();
	glPushMatrix();
	// 다리2
	glTranslatef(0, -3, 0);
	glTranslatef(0, 0, -1.5);
	glScalef(0.3, 1.0, 0.3);
	glColor3f(1.0f, 1.0f, 1.0f);
	glutSolidCube(4);

	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	//몸통
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[6]);
	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glColor3f(1.0f, 1.0f, 1.0f);
	glScalef(0.8, 1.0, 1);
	glutSolidCube(5);

	glDisable(GL_TEXTURE_2D);
	glPopMatrix();

	// 팔1
	glPushMatrix();
	glTranslatef(1, 1, 0);
	glRotatef(-60, 1, 0, 0);
	glRotatef(-30, 0, 0, 1);
	//윗팔

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[6]);
	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glTranslatef(0, 0, 3.5);
	glScalef(0.3, 0.5, 0.3);
	glColor3f(1.0f, 1.0f, 1.0f);
	glutSolidCube(4);

	glDisable(GL_TEXTURE_2D);
	//아랫팔
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[5]);
	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glTranslatef(0, 4, 0);
	glutSolidCube(4);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(1, 1, 0);
	// 팔2
	//윗팔
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[6]);
	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glRotatef(60, 1, 0, 0);
	glRotatef(-30, 0, 0, 1);
	glTranslatef(0, 0, -3.5);
	glScalef(0.3, 0.5, 0.3); glColor3f(1.0f, 1.0f, 1.0f);
	glutSolidCube(4);

	glDisable(GL_TEXTURE_2D);
	//아랫팔
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[5]);
	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glTranslatef(0, 4, 0);
	glutSolidCube(4);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();

	// 머리
	glTranslatef(0, 3, 0);
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[5]);
	glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glTranslatef(0, 1, 0);
	glColor3f(1.0f, 1.0f, 1.0f);
	glutSolidCube(3);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	//코
	glPushMatrix();
	glTranslatef(2, 1, 0);
	glScalef(0.3, 0.3, 0.3);
	glColor3f(1.0f, 0, 0);
	glutSolidCube(2);
	glPopMatrix();
	glPopMatrix();
}

//글 출력
void Drawtext(int x, int y, int z, char string[])
{

	glRasterPos3f(x, y, z);//위치
	int len = (int)strlen(string); //불러온 문자열의 길이

	for (int i = 0; i < len; i++)
	{
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, string[i]);
	}
}
//직각 투영-원근투영
void Viewfunc()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (View)
	{
		// 투영 행렬 스택 재설정
		gluPerspective(60.0, (double)800 / (double)600, 1.0, 10000.0);
		glTranslatef(0.0, 0.0, -300.0);
		// 모델 뷰 행렬 스택 재설정
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

	}
	else
	{

		glOrtho(-400.0, 400.0, -300.0, 300.0, -400.0, 400.0); //직각 투영

	}
}
GLvoid Reshape(int w, int h)
{
	//--- 뷰포트 변환 설정
	glViewport(0, 0, w, h);

	glOrtho(-400.0, 400.0, -300.0, 300.0, -400.0, 400.0); //직각 투영


}
void FileInput() //파일 불러오기
{
	rfp = fopen("Save_Num.txt", "r");
	rfp2 = fopen("Ranking.txt", "r");
	fscanf(rfp, "%d", &rank_num);
	if (rank_num == 0)
	{
		for (int i = 0; i < RANK_LIM; i++)
		{
			Rank_Score[i] = 0;
			Rank_grade[i] = 0;
		}
	}
	else
	{
		for (int i = 0; i < rank_num; i++)
		{
			fscanf(rfp2, "%d %d\n", &Rank_Score[i], &Rank_grade[i]);
		}
		for (int i = 0; i < rank_num; i++)
		{
			printf("%d :%d %d\n", i + 1, Rank_Score[i], Rank_grade[i]);
		}
	}
	fclose(rfp);
	fclose(rfp2);

}
void FileOutput()//파일 내보내기
{
	int Temp = 0;
	wfp = fopen("Save_Num.txt", "w");
	wfp2 = fopen("Ranking.txt", "w");
	fprintf(wfp, "%d", rank_num);
	if (rank_num == 0)
	{
		for (int i = 0; i < RANK_LIM; i++)
		{
			fprintf(wfp2, "%d %d\n", Rank_Score[i], Rank_grade[i]);
		}
	}
	else
	{
		for (int j = 0; j < rank_num; j++)
		{
			for (int i = 0; i < rank_num; i++)
			{
				if (Rank_Score[i] < Rank_Score[i + 1])
				{
					Temp = Rank_Score[i];
					Rank_Score[i] = Rank_Score[i + 1];
					Rank_Score[i + 1] = Temp;
					Temp = Rank_grade[i];
					Rank_grade[i] = Rank_grade[i + 1];
					Rank_grade[i + 1] = Temp;
				}
			}
		}
		for (int i = 0; i < rank_num; i++)
		{
			fprintf(wfp2, "%d %d\n", Rank_Score[i], Rank_grade[i]);
		}
	}
	fclose(wfp);
	fclose(wfp2);
}
void Score() //점수 만드는 함수
{
	glPushMatrix();
	glTranslatef(200, 100, 0);
	glColor3f(1, 1, 1);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[0]);
	glBegin(GL_QUADS);
	glTexCoord2f(Text_score_S[0] / 10 - 0.1, 1);
	glVertex3f(-10, 10, 0);
	glTexCoord2f(Text_score_S[0] / 10, 1);
	glVertex3f(10, 10, 0);
	glTexCoord2f(Text_score_S[0] / 10, 0);
	glVertex3f(10, -10, 0);
	glTexCoord2f(Text_score_S[0] / 10 - 0.1, 0);
	glVertex3f(-10, -10, 0);
	glEnd();
	glDisable(GL_TEXTURE_2D);


	glPushMatrix();
	glTranslatef(-20, 0, 0);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[0]);
	glBegin(GL_QUADS);
	glTexCoord2f(Text_score_S[1] / 10 - 0.1, 1);
	glVertex3f(-10, 10, 0);
	glTexCoord2f(Text_score_S[1] / 10, 1);
	glVertex3f(10, 10, 0);
	glTexCoord2f(Text_score_S[1] / 10, 0);
	glVertex3f(10, -10, 0);
	glTexCoord2f(Text_score_S[1] / 10 - 0.1, 0);
	glVertex3f(-10, -10, 0);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-40, 0, 0);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[0]);
	glBegin(GL_QUADS);
	glTexCoord2f(Text_score_S[2] / 10 - 0.1, 1);
	glVertex3f(-10, 10, 0);
	glTexCoord2f(Text_score_S[2] / 10, 1);
	glVertex3f(10, 10, 0);
	glTexCoord2f(Text_score_S[2] / 10, 0);
	glVertex3f(10, -10, 0);
	glTexCoord2f(Text_score_S[2] / 10 - 0.1, 0);
	glVertex3f(-10, -10, 0);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-60, 0, 0);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[0]);
	glBegin(GL_QUADS);
	glTexCoord2f(Text_score_S[3] / 10 - 0.1, 1);
	glVertex3f(-10, 10, 0);
	glTexCoord2f(Text_score_S[3] / 10, 1);
	glVertex3f(10, 10, 0);
	glTexCoord2f(Text_score_S[3] / 10, 0);
	glVertex3f(10, -10, 0);
	glTexCoord2f(Text_score_S[3] / 10 - 0.1, 0);
	glVertex3f(-10, -10, 0);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-80, 0, 0);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[0]);
	glBegin(GL_QUADS);
	glTexCoord2f(Text_score_S[4] / 10 - 0.1, 1);
	glVertex3f(-10, 10, 0);
	glTexCoord2f(Text_score_S[4] / 10, 1);
	glVertex3f(10, 10, 0);
	glTexCoord2f(Text_score_S[4] / 10, 0);
	glVertex3f(10, -10, 0);
	glTexCoord2f(Text_score_S[4] / 10 - 0.1, 0);
	glVertex3f(-10, -10, 0);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	glPushMatrix();
	glTranslatef(-100, 0, 0);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[0]);
	glBegin(GL_QUADS);
	glTexCoord2f(Text_score_S[5] / 10 - 0.1, 1);
	glVertex3f(-10, 10, 0);
	glTexCoord2f(Text_score_S[5] / 10, 1);
	glVertex3f(10, 10, 0);
	glTexCoord2f(Text_score_S[5] / 10, 0);
	glVertex3f(10, -10, 0);
	glTexCoord2f(Text_score_S[5] / 10 - 0.1, 0);
	glVertex3f(-10, -10, 0);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	//
	//glRectf(-10, -10, 10, 10);

	//glTranslatef(20, 0, 0);
	//glRectf(-10, -10, 10, 10);

	//glTranslatef(20, 0, 0);
	//glRectf(-10, -10, 10, 10);

	//glTranslatef(20, 0, 0);
	//glRectf(-10, -10, 10, 10);

	//glTranslatef(20, 0, 0);
	//glRectf(-10, -10, 10, 10);

	//glTranslatef(20, 0, 0);
	//glRectf(-10, -10, 10, 10);
	glPopMatrix();

}
GLubyte * LoadDIBitmap(const char *filename, BITMAPINFO **info)
{
	FILE *fp;
	GLubyte *bits;
	int bitsize, infosize;
	BITMAPFILEHEADER header;
	// 바이너리 읽기 모드로 파일을 연다
	if ((fp = fopen(filename, "rb")) == NULL)
		return NULL;
	// 비트맵 파일 헤더를 읽는다.
	if (fread(&header, sizeof(BITMAPFILEHEADER), 1, fp) < 1) {
		fclose(fp);
		return NULL;
	}
	// 파일이 BMP 파일인지 확인한다.
	if (header.bfType != 'MB') {
		fclose(fp);
		return NULL;
	}
	// BITMAPINFOHEADER 위치로 간다.
	infosize = header.bfOffBits - sizeof(BITMAPFILEHEADER);
	// 비트맵 이미지 데이터를 넣을 메모리 할당을 한다.
	if ((*info = (BITMAPINFO *)malloc(infosize)) == NULL) {
		fclose(fp);
		exit(0);
		return NULL;
	}
	// 비트맵 인포 헤더를 읽는다.
	if (fread(*info, 1, infosize, fp) < (unsigned int)infosize) {
		free(*info);
		fclose(fp);
		return NULL;
	}
	// 비트맵의 크기 설정
	if ((bitsize = (*info)->bmiHeader.biSizeImage) == 0)
		bitsize = ((*info)->bmiHeader.biWidth*(*info)->bmiHeader.biBitCount + 7) / 8.0 * abs((*info)->bmiHeader.biHeight);
	// 비트맵의 크기만큼 메모리를 할당한다.
	if ((bits = (unsigned char *)malloc(bitsize)) == NULL) {
		free(*info);
		fclose(fp);
		return NULL;
	}
	// 비트맵 데이터를 bit(GLubyte 타입)에 저장한다.
	if (fread(bits, 1, bitsize, fp) < (unsigned int)bitsize) {
		free(*info); free(bits);
		fclose(fp);
		return NULL;
	}
	fclose(fp);
	return bits;
}
void Texture_make()
{
	glPushMatrix();
	glGenTextures(TEXTURE_NUMBER, textures);

	glBindTexture(GL_TEXTURE_2D, textures[0]); // 텍스처링 할 객체에 텍스처를 연결해준다.
	pBytes = LoadDIBitmap("resource\\number_b.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 432, 40, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[1]); // 텍스처링 할 객체에 텍스처를 연결해준다.
	pBytes = LoadDIBitmap("resource\\Rank.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 864, 100, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[2]); // 텍스처링 할 객체에 텍스처를 연결해준다.
	pBytes = LoadDIBitmap("resource\\menu.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 1000, 1000, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[3]); // 텍스처링 할 객체에 텍스처를 연결해준다.
	pBytes = LoadDIBitmap("resource\\background.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 960, 540, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[4]); // 텍스처링 할 객체에 텍스처를 연결해준다.
	pBytes = LoadDIBitmap("resource\\Title.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 1140, 487, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[5]); // 텍스처링 할 객체에 텍스처를 연결해준다.
	pBytes = LoadDIBitmap("resource\\skin.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 461, 307, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[6]); // 텍스처링 할 객체에 텍스처를 연결해준다.
	pBytes = LoadDIBitmap("resource\\clothes.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 461, 307, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[7]); // 텍스처링 할 객체에 텍스처를 연결해준다.
	pBytes = LoadDIBitmap("resource\\pants.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 78, 94, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[8]); // 텍스처링 할 객체에 텍스처를 연결해준다.
	pBytes = LoadDIBitmap("resource\\tiles.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 359, 350, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[9]); // 텍스처링 할 객체에 텍스처를 연결해준다.
	pBytes = LoadDIBitmap("resource\\run.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 359, 350, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[10]); // 텍스처링 할 객체에 텍스처를 연결해준다.
	pBytes = LoadDIBitmap("resource\\homework.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 361, 350, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[11]); // 텍스처링 할 객체에 텍스처를 연결해준다.
	pBytes = LoadDIBitmap("resource\\rest.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 359, 350, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[12]); // 텍스처링 할 객체에 텍스처를 연결해준다.
	pBytes = LoadDIBitmap("resource\\fest.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 359, 350, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[13]); // 텍스처링 할 객체에 텍스처를 연결해준다.
	pBytes = LoadDIBitmap("resource\\drink.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 359, 350, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[14]); // 텍스처링 할 객체에 텍스처를 연결해준다.
	pBytes = LoadDIBitmap("resource\\end.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 359, 350, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[15]); // 텍스처링 할 객체에 텍스처를 연결해준다.
	pBytes = LoadDIBitmap("resource\\Ranking.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 600, 560, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[16]); // 텍스처링 할 객체에 텍스처를 연결해준다.
	pBytes = LoadDIBitmap("resource\\50.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 359, 350, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, GL_MODULATE);
}
void Rank_Func()
{
	if (SCORE > 150000) RANK = 9; //a+
	else if (SCORE <= 150000 && SCORE > 100000) RANK = 8;//a0
	else if (SCORE <= 100000 && SCORE > 80000) RANK = 7;//b+
	else if (SCORE <= 80000 && SCORE > 60000) RANK = 6;//b0
	else if (SCORE <= 70000 && SCORE > 40000) RANK = 5;//c+
	else if (SCORE <= 60000 && SCORE > 20000) RANK = 4;//c0
	else if (SCORE <= 40000 && SCORE > 10000) RANK = 3;//d+
	else if (SCORE <= 30000 && SCORE > 5000) RANK = 2;//d0
	else if (SCORE <= 5000) RANK = 0;//f
}
void Rank_View()
{
	glPushMatrix();
	glTranslatef(-100, 0, 0);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textures[1]);
	glBegin(GL_QUADS);
	if (RANK == 0)
	{
		glColor3f(0.8, 0.8, 0.8);
		glTexCoord2f(0.94, 0);
		glVertex3f(-100, -100, 0);
		glTexCoord2f(0.94, 1);
		glVertex3f(-100, 100, 0);
		glTexCoord2f(1, 1);
		glVertex3f(100, 100, 0);
		glTexCoord2f(1, 0);
		glVertex3f(100, -100, 0);
	}
	else if (RANK == 1)
	{
		glColor3f(0, 0.8, 0.8);
		glTexCoord2f(0.82, 0);
		glVertex3f(-100, -100, 0);
		glTexCoord2f(0.82, 1);
		glVertex3f(-100, 100, 0);
		glTexCoord2f(0.94, 1);
		glVertex3f(100, 100, 0);
		glTexCoord2f(0.94, 0);
		glVertex3f(100, -100, 0);
	}
	else if (RANK == 2)
	{
		glColor3f(0, 1, 1);
		glTexCoord2f(0.7, 0);
		glVertex3f(-100, -100, 0);
		glTexCoord2f(0.7, 1);
		glVertex3f(-100, 100, 0);
		glTexCoord2f(0.82, 1);
		glVertex3f(100, 100, 0);
		glTexCoord2f(0.82, 0);
		glVertex3f(100, -100, 0);
	}
	else if (RANK == 3)
	{
		glColor3f(1, 0, 1);
		glTexCoord2f(0.58, 0);
		glVertex3f(-100, -100, 0);
		glTexCoord2f(0.58, 1);
		glVertex3f(-100, 100, 0);
		glTexCoord2f(0.7, 1);
		glVertex3f(100, 100, 0);
		glTexCoord2f(0.7, 0);
		glVertex3f(100, -100, 0);
	}
	else if (RANK == 4)
	{
		glColor3f(1, 1, 0);
		glTexCoord2f(0.46, 0);
		glVertex3f(-100, -100, 0);
		glTexCoord2f(0.46, 1);
		glVertex3f(-100, 100, 0);
		glTexCoord2f(0.58, 1);
		glVertex3f(100, 100, 0);
		glTexCoord2f(0.58, 0);
		glVertex3f(100, -100, 0);
	}
	else if (RANK == 5)
	{
		glColor3f(0, 0, 1);
		glTexCoord2f(0.36, 0);
		glVertex3f(-100, -100, 0);
		glTexCoord2f(0.36, 1);
		glVertex3f(-100, 100, 0);
		glTexCoord2f(0.46, 1);
		glVertex3f(100, 100, 0);
		glTexCoord2f(0.46, 0);
		glVertex3f(100, -100, 0);
	}
	else if (RANK == 6)
	{
		glColor3f(0, 1, 0);
		glTexCoord2f(0.24, 0);
		glVertex3f(-100, -100, 0);
		glTexCoord2f(0.24, 1);
		glVertex3f(-100, 100, 0);
		glTexCoord2f(0.36, 1);
		glVertex3f(100, 100, 0);
		glTexCoord2f(0.36, 0);
		glVertex3f(100, -100, 0);
	}
	else if (RANK == 7)
	{
		glColor3f(0.8, 0, 0);
		glTexCoord2f(0.12, 0);
		glVertex3f(-100, -100, 0);
		glTexCoord2f(0.12, 1);
		glVertex3f(-100, 100, 0);
		glTexCoord2f(0.24, 1);
		glVertex3f(100, 100, 0);
		glTexCoord2f(0.24, 0);
		glVertex3f(100, -100, 0);
	}
	else if (RANK == 8)
	{
		glColor3f(1, 0, 0);
		glTexCoord2f(0, 0);
		glVertex3f(-100, -100, 0);
		glTexCoord2f(0, 1);
		glVertex3f(-100, 100, 0);
		glTexCoord2f(0.1, 1);
		glVertex3f(100, 100, 0);
		glTexCoord2f(0.1, 0);
		glVertex3f(100, -100, 0);
	}
	glEnd();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}